module.exports = {
    "extends": "stylelint-config-standard-scss",
    "customSyntax": "postcss-scss",
    "rules": {
        "declaration-block-trailing-semicolon": null,
        "no-descending-specificity": null,
        "no-empty-source": null
    }
}
