import React from 'react'

const Home = (props) => {
  const { name } = props

  return (
        <div>
            <h1>Home page</h1>
            <p>Welcome, {name}!</p>
        </div>
  )
}

export default Home
