const mix = require('laravel-mix')
require('laravel-mix-eslint')
require('@jorenrothman/laravel-mix-stylelint')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
  .react()
  .eslint()
  .postCss('resources/css/app.css', 'public/css', [
    //
  ])
  .stylelint({
    files: ['resources/**/*.scss', 'resources/**/*.css']
  })
